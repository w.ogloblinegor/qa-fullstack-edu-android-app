package com.example.mobileandroidinvestingedu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import khttp.options

class MainActivity : AppCompatActivity() {
    //var wiremock = WireMockServer(wireMockConfig().port(8080).bindAddress("localhost"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clear()
        //wiremock.start()
    }

    fun sendRequest(view: View) {
        val intent = Intent(this@MainActivity, NResponseActivity::class.java)
        val url = findViewById<TextView>(R.id.editText).text.toString()
        val params = getParams()
        val headers = getHeaders()
        val radioMethod = findViewById<RadioGroup>(R.id.radioMethod)
        val method = findViewById<RadioButton>(radioMethod.checkedRadioButtonId)

        intent.putExtra("requestType", method.text)
        intent.putExtra("params", params)
        intent.putExtra("headers", headers)
        intent.putExtra("url", url)
        startActivity(intent)
    }

    private fun getParams(): HashMap<String, String>? {
        return intent.getSerializableExtra("params") as? HashMap<String, String>
    }

    private fun getHeaders(): HashMap<String, String>? {
        return intent.getSerializableExtra("headers") as? HashMap<String, String>
    }

    fun editParams(view: View) {
        val intent = Intent(this@MainActivity, ParamsActivity::class.java)
        val url = findViewById<TextView>(R.id.editText).text.toString()
        intent.putExtra("url", url)
        startActivity(intent)
    }

    private fun clear() {
        findViewById<RadioGroup>(R.id.radioMethod).clearCheck()
    }
}